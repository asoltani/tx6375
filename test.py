import argparse
import os

import torch

from ddpg.nets import Critic, Actor
from environment.env_wrappers import TXEnv, SkipFramesWrapper
from train import restore_args
from utils.misc_util import str2params

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--logdir', type=str, default=None)
    args = parser.parse_args()

    args.restore_args_from = os.path.join(args.logdir, "args.json")
    args = restore_args(args)

    env = TXEnv(highknee=args.highknee, visualize=True, integrator_accuracy=0.00001)
    env = SkipFramesWrapper(env, args.skip_frames)

    args.n_action = env.action_space.shape[0]
    args.n_observation = env.observation_space.shape[0]

    args.actor_layers = str2params(args.actor_layers)
    args.critic_layers = str2params(args.critic_layers)

    actor = Actor(args.n_observation, args.n_action, args.actor_layers)
    critic = Critic(args.n_observation, args.n_action, args.critic_layers)
    actor.load_state_dict(torch.load(os.path.join(args.logdir, "actor_state_dict.pkl")))
    critic.load_state_dict(torch.load(os.path.join(args.logdir, "critic_state_dict.pkl")))

    obs = env.reset()
    n_frame = 0

    while True:
        n_frame += 1
        action = actor(torch.tensor(obs).float()).data.numpy()
        obs, rew, done, _ = env.step(action)
        print(n_frame, rew)

        if done:
            break

    env.close()
