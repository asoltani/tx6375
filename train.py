import argparse
import copy
import json
import os
from multiprocessing import Value

import torch
import torch.multiprocessing as mp

from common.loss import create_loss
from ddpg.model import train_single_thread, play_single_thread, save_fn
from ddpg.nets import Actor, Critic
from environment.env_wrappers import create_env
from utils.misc_util import str2params, create_if_need
from utils.torch_util import hard_update


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--seed', type=int, default=42)
    parser.add_argument("--integrator-accuracy", type=float, default=1e-5)
    parser.add_argument('--logdir', type=str, default="./logs")
    parser.add_argument('--num-threads', type=int, default=3)
    parser.add_argument('--skip-frames', type=int, default=5)
    for agent in ["actor", "critic"]:
        parser.add_argument('--{}-layers'.format(agent), type=str, default="64-128-64")
        parser.add_argument('--{}-lr'.format(agent), type=float, default=1e-3)
        parser.add_argument('--restore-{}-from'.format(agent), type=str, default=None)
    parser.add_argument('--gamma', type=float, default=0.96)
    parser.add_argument('--tau', default=0.001, type=float)
    parser.add_argument('--train-steps', type=int, default=int(1e5))
    parser.add_argument('--batch-size', type=int, default=256)  # per worker
    parser.add_argument('--buffer-size', type=int, default=int(1e6))
    parser.add_argument('--max-episodes', default=int(1e4), type=int)
    parser.add_argument('--restore-args-from', type=str, default=None)
    parser.add_argument('--highknee', type=bool, default=False)

    return parser.parse_args()


def restore_args(args):
    with open(args.restore_args_from, "r") as fin:
        params = json.load(fin)

    del params["logdir"]
    del params["restore_args_from"]

    for key, value in params.items():
        setattr(args, key, value)
    return args


if __name__ == '__main__':
    os.environ['OMP_NUM_THREADS'] = '1'
    torch.set_num_threads(1)
    args = parse_args()

    create_if_need(args.logdir)

    if args.restore_args_from is not None:
        args = restore_args(args)

    with open("{}/args.json".format(args.logdir), "w") as fout:
        json.dump(vars(args), fout, indent=4, ensure_ascii=False, sort_keys=True)

    env = create_env(args.highknee, args.integrator_accuracy, args.skip_frames)

    args.n_action = env.action_space.shape[0]
    args.n_observation = env.observation_space.shape[0]
    args.actor_layers = str2params(args.actor_layers)
    args.critic_layers = str2params(args.critic_layers)

    actor = Actor(args.n_observation, args.n_action, args.actor_layers)
    critic = Critic(args.n_observation, args.n_action, args.critic_layers)

    if args.restore_actor_from is not None:
        actor.load_state_dict(torch.load(args.restore_actor_from))
    if args.restore_critic_from is not None:
        critic.load_state_dict(torch.load(args.restore_critic_from))

    actor.train()
    critic.train()
    actor.share_memory()
    critic.share_memory()

    target_actor = copy.deepcopy(actor)
    target_critic = copy.deepcopy(critic)

    hard_update(target_actor, actor)
    hard_update(target_critic, critic)

    target_actor.train()
    target_critic.train()
    target_actor.share_memory()
    target_critic.share_memory()

    actor_optim = torch.optim.Adam(actor.parameters(), lr=args.actor_lr)
    critic_optim = torch.optim.Adam(critic.parameters(), lr=args.critic_lr)

    criterion = create_loss()

    num_threads = args.num_threads
    processes = []
    best_reward = Value("f", 0.0)
    try:
        global_episode = Value("i", 0)
        global_update_step = Value("i", 0)
        episodes_queue = mp.Queue()
        for rank in range(num_threads):
            args.thread = rank
            if rank == 0:
                p = mp.Process(
                    target=train_single_thread,
                    args=(actor, critic, target_actor, target_critic, actor_optim, critic_optim, criterion,
                          args, global_episode, global_update_step, episodes_queue))
            else:
                p = mp.Process(
                    target=play_single_thread,
                    args=(actor, critic, target_actor, target_critic, args, global_episode,
                          episodes_queue, best_reward))
            p.start()
            processes.append(p)

        for p in processes:
            p.join()
    except KeyboardInterrupt:
        pass

    save_fn(actor, critic, target_actor, target_critic, args.logdir)
