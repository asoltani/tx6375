# TX 6375

Ce repo contient les codes de l'agent et de l'environnement de RL utilisés lors de la 
TX 6375 "Reinforcement learning for medical application related to human locomotion", 
ainsi que le rapport de TX dans le dossier *rapport*.

Afin d'entraîner l'agent il faut préalablement installer l'environnement conda:
```bash
bash setup_env.sh
```
L'entraînement se fait ensuite en deux parties.  
Premièrement, l'entraînement à la stabilisation:
```bash
PYTHONPATH=. CUDA_VISIBLE_DEVICES="" python train.py --logdir ./logs_stand --num-thread 8 --skip-frames 5 --actor-layers 64-128-64 --actor-lr 0.001 --critic-layers 64-128-32 
```
Deuxièmement l'entraînement au mouvement de levé de genoux: 
```bash
PYTHONPATH=. CUDA_VISIBLE_DEVICES="" python train.py --logdir ./logs_highknee --num-thread 8 --skip-frames 5 --actor-layers 64-128-64 --actor-lr 0.001 --critic-layers 64-128-32 
```
