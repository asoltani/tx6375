import os
import random
from itertools import tee

import numpy as np


def create_if_need(path):
    if not os.path.exists(path):
        os.makedirs(path)


def str2params(string, delimeter="-"):
    try:
        result = list(map(int, string.split(delimeter)))
    except:
        result = None
    return result


def set_global_seeds(i):
    try:
        import torch
    except ImportError:
        pass
    else:
        torch.manual_seed(i)
    try:
        import tensorflow as tf
    except ImportError:
        pass
    else:
        tf.set_random_seed(i)
    np.random.seed(i)
    random.seed(i)


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)
