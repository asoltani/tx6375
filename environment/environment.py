import math
import os

import numpy as np
from osim.env.osim import OsimEnv


class TXEnv(OsimEnv):
    model_path = os.path.join(os.path.dirname(__file__), '../opensim_models/gait9dof18musc.osim')
    time_limit = 1000
    error_max = 0.6
    reward_threshold = 0.1

    target_pelvis = [0, 0.9, 0]
    target_toes_l = [0, 0, -0.1]
    target_toes_r = [0, 0, 0.1]

    blending_alpha = 0.01

    target_toes_r_end = [0, 0.5, 0.1]

    freedom = 0.2

    def __init__(self, highknee=True, visualize=False, integrator_accuracy=0.1):
        super(TXEnv, self).__init__(visualize=visualize, integrator_accuracy=integrator_accuracy)
        self.highknee = highknee
        self.osim_model.model.initSystem()

    def get_observation_dict(self):
        state_desc = self.get_state_desc()

        obs_dict = {}

        obs_dict["mass_center_pos"] = state_desc["misc"]["mass_center_pos"]
        obs_dict["mass_center_vel"] = state_desc["misc"]["mass_center_vel"]

        obs_dict["pelvis_pos"] = state_desc["body_pos"]["pelvis"]
        obs_dict["pelvis_vel"] = state_desc["body_vel"]["pelvis"]
        obs_dict["toes_l_pos"] = state_desc["body_pos"]["toes_l"]
        obs_dict["toes_r_pos"] = state_desc["body_pos"]["toes_r"]
        obs_dict["toes_l_vel"] = state_desc["body_vel"]["toes_l"]
        obs_dict["toes_r_vel"] = state_desc["body_vel"]["toes_r"]

        obs_dict["target_pelvis"] = self.target_pelvis
        obs_dict["target_calcn_l"] = self.target_pelvis
        obs_dict["target_calcn_r"] = self.target_pelvis

        muscle_dict = {}
        for muscle_name, muscle_values in state_desc['muscles'].items():
            muscle_desc = {"fiber_force": muscle_values["fiber_force"],
                           "fiber_length": muscle_values["fiber_length"],
                           "fiber_velocity": muscle_values["fiber_velocity"]}
            muscle_dict[muscle_name] = muscle_desc

        obs_dict["muscles"] = muscle_dict

        return obs_dict

    def get_observation(self):
        obs_dict = self.get_observation_dict()

        res = []
        res += obs_dict["target_pelvis"]
        res += obs_dict["target_calcn_l"]
        res += obs_dict["target_calcn_r"]
        res += obs_dict["mass_center_pos"]
        res += obs_dict["mass_center_vel"]
        res += obs_dict["pelvis_pos"]
        res += obs_dict["pelvis_vel"]
        res += obs_dict["toes_l_pos"]
        res += obs_dict["toes_l_vel"]
        res += obs_dict["toes_r_pos"]
        res += obs_dict["toes_r_vel"]
        for muscle_value in obs_dict["muscles"].values():
            res.append(muscle_value["fiber_force"])
            res.append(muscle_value["fiber_length"])
            res.append(muscle_value["fiber_velocity"])

        return np.array(res).astype(np.float32)

    def get_observation_space_size(self):
        return 87

    def reset(self, project=True, obs_as_dict=False):
        obs = super(TXEnv, self).reset(project=project, obs_as_dict=obs_as_dict)
        self.target_pelvis = [0, 0.9, 0]
        self.target_toes_l = [0, 0, -0.1]
        self.target_toes_r = [0, 0, 0.1]
        self.osim_model.reset_manager()

        return obs

    def get_errors(self):
        pelvis_pos = self.get_observation_dict()['pelvis_pos']
        toes_l_pos = self.get_observation_dict()['toes_l_pos']
        toes_r_pos = self.get_observation_dict()['toes_r_pos']
        error_pelvis = math.sqrt(
            (self.target_pelvis[0] - pelvis_pos[0]) ** 2 + (self.target_pelvis[1] - pelvis_pos[1]) ** 2 +
            (self.target_pelvis[2] - pelvis_pos[2]) ** 2)
        error_calcn_l = math.sqrt(
            (self.target_toes_l[0] - toes_l_pos[0]) ** 2 + (self.target_toes_l[1] - toes_l_pos[1]) ** 2 +
            (self.target_toes_l[2] - toes_l_pos[2]) ** 2)
        error_calcn_r = math.sqrt(
            (self.target_toes_r[0] - toes_r_pos[0]) ** 2 + (self.target_toes_r[1] - toes_r_pos[1]) ** 2 +
            (self.target_toes_r[2] - toes_r_pos[2]) ** 2)
        return error_pelvis, error_calcn_l, error_calcn_r

    def is_done(self):
        error_pelvis, error_calcn_l, error_calcn_r = self.get_errors()
        return error_pelvis > 2 * self.freedom or error_calcn_r > 2 * self.freedom or error_calcn_l > 2 * self.freedom

    def get_reward(self):
        if self.is_done():
            return -10

        error_pelvis, error_calcn_l, error_calcn_r = self.get_errors()

        if error_pelvis < self.freedom and error_calcn_r < self.freedom and error_calcn_l < self.freedom:
            return 1
        return 1 - error_pelvis - error_calcn_r - error_calcn_l

    def step(self, action, project=True, obs_as_dict=False):
        if self.highknee:
            self.target_toes_r = list(
                np.array(self.target_toes_r_end) * self.blending_alpha + np.array(self.target_toes_r) * (
                        1 - self.blending_alpha))
        return super(TXEnv, self).step(action, project=project, obs_as_dict=obs_as_dict)


if __name__ == '__main__':
    env = TXEnv(visualize=True, integrator_accuracy=0.1)
    obs = env.reset(obs_as_dict=False)
    frame_idx = 0
    total_reward = 0
    rewards = []

    # pprint(obs)
    # print(env.action_space)

    while True:
        frame_idx += 1
        obs, reward, is_done, _ = env.step(np.zeros(env.action_space.shape))
        rewards.append(reward)
        total_reward += reward
        # print(frame_idx,  total_reward, reward)
        print(reward)

        if is_done:
            break

    # plt.plot(rewards)
    # plt.show()
    env.close()
