import gym

from environment.environment import TXEnv


class SkipFramesWrapper(gym.Wrapper):
    def __init__(self, env: gym.Env, skip_frames: int):
        gym.Wrapper.__init__(self, env)
        self.skip_frames = skip_frames

    def step(self, action):
        total_reward = 0.
        for _ in range(self.skip_frames):
            observation, reward, done, _ = self.env.step(action)
            total_reward += reward
            if done:
                break

        return observation, total_reward, done, None


def create_env(highknee: bool, integrator_accuracy: float, skip_frames: int):
    env = TXEnv(highknee=highknee, visualize=False, integrator_accuracy=integrator_accuracy)
    return SkipFramesWrapper(env, skip_frames)
