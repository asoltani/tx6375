from collections import OrderedDict

import torch
import torch.nn as nn

from utils.misc_util import pairwise

class LayerNorm(nn.Module):
    def __init__(self, features, eps=1e-6):
        super().__init__()
        self.gamma = nn.Parameter(torch.ones(features))
        self.beta = nn.Parameter(torch.zeros(features))
        self.eps = eps

    def forward(self, x):
        mean = x.mean(-1, keepdim=True)
        std = x.std(-1, keepdim=True)
        return self.gamma * (x - mean) / (std + self.eps) + self.beta


class LinearNormNet(nn.Module):
    def __init__(self, layers, activation, linear_layer=nn.Linear):
        super(LinearNormNet, self).__init__()
        self.input_shape = layers[0]
        self.output_shape = layers[-1]

        layer_fn = lambda layer: [
            ("linear_{}".format(layer[0]), linear_layer(layer[1][0], layer[1][1])),
            ("layer_norm_{}".format(layer[0]), LayerNorm(layer[1][1])),
            ("act_{}".format(layer[0]), activation())]

        self.net = torch.nn.Sequential(
            OrderedDict([
                x for y in map(
                    lambda layer: layer_fn(layer),
                    enumerate(pairwise(layers))) for x in y]))

    def forward(self, x):
        x = self.net.forward(x)
        return x

class LinearNet(nn.Module):
    def __init__(self, layers, activation, linear_layer=nn.Linear):
        super(LinearNet, self).__init__()
        self.input_shape = layers[0]
        self.output_shape = layers[-1]

        layer_fn = lambda layer: [
            ("linear_{}".format(layer[0]), linear_layer(layer[1][0], layer[1][1])),
            ("act_{}".format(layer[0]), activation())]

        self.net = torch.nn.Sequential(
            OrderedDict([
                x for y in map(
                    lambda layer: layer_fn(layer),
                    enumerate(pairwise(layers))) for x in y]))

    def forward(self, x):
        x = self.net.forward(x)
        return x


class Actor(nn.Module):
    def __init__(self, n_observation, n_action, layers):
        super(Actor, self).__init__()

        linear_layer = nn.Linear

        self.feature_net = LinearNormNet(
            layers=[n_observation] + layers,
            activation=torch.nn.ReLU,
            linear_layer=linear_layer)

        self.policy_net = LinearNet(
            layers=[self.feature_net.output_shape, n_action],
            activation=torch.nn.Tanh,
        )

    def forward(self, observation):
        x = observation
        x = self.feature_net.forward(x)
        x = self.policy_net.forward(x)
        return x


class Critic(nn.Module):
    def __init__(self, n_observation, n_action, layers):
        super(Critic, self).__init__()

        linear_layer = nn.Linear

        self.feature_net = LinearNormNet(
            layers=[n_observation + n_action] + layers,
            activation=torch.nn.ReLU,
            linear_layer=linear_layer)
        self.value_net = nn.Linear(self.feature_net.output_shape, 1)

    def forward(self, observation, action):
        x = torch.cat((observation, action), dim=1)
        x = self.feature_net.forward(x)
        x = self.value_net.forward(x)
        return x
