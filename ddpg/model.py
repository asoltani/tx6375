import queue as py_queue
import random
import time

import numpy as np
import torch

from buffer.buffers import create_buffer
from common.loss import create_cycle_decay_fn, create_linear_decay_fn
from common.random_process import create_ou_process
from environment.env_wrappers import create_env
from utils.logger import Logger
from utils.misc_util import create_if_need, set_global_seeds
from utils.torch_util import to_numpy, to_tensor, soft_update


def save_fn(actor, critic, target_actor, target_critic, logdir, episode=None):
    if episode is None:
        save_path = logdir
    else:
        save_path = "{}/episode_{}".format(logdir, episode)
        create_if_need(save_path)
    torch.save(actor.state_dict(), "{}/actor_state_dict.pkl".format(save_path))
    torch.save(critic.state_dict(), "{}/critic_state_dict.pkl".format(save_path))
    torch.save(target_actor.state_dict(), "{}/target_actor_state_dict.pkl".format(save_path))
    torch.save(target_critic.state_dict(), "{}/target_critic_state_dict.pkl".format(save_path))


def act_fn(actor, observation, noise=0):
    action = to_numpy(actor(to_tensor(np.array([observation], dtype=np.float32)))).squeeze(0)
    action += noise
    action = np.clip(action, -1, 1)
    return action


def update_fn(actor, critic, target_actor, target_critic, actor_optim, critic_optim, criterion,
              observations, actions, rewards, next_observations, dones, weights, tau):
    dones = dones[:, None].astype(np.bool)
    rewards = rewards[:, None].astype(np.float32)

    dones = to_tensor(np.invert(dones).astype(np.float32))
    rewards = to_tensor(rewards)
    weights = to_tensor(weights, requires_grad=False)

    with torch.no_grad():
        next_v_values = target_critic(
            to_tensor(next_observations, volatile=True),
            target_actor(to_tensor(next_observations, volatile=True)),
        )

    reward_predicted = dones * 0.96 * next_v_values
    td_target = rewards + reward_predicted

    # Critic update
    critic.zero_grad()

    v_values = critic(to_tensor(observations), to_tensor(actions))
    value_loss = criterion(v_values, td_target, weights=weights)
    value_loss.backward()

    torch.nn.utils.clip_grad_norm(critic.parameters(), 10.)
    critic_optim.step()

    # Actor update
    actor.zero_grad()

    policy_loss = -critic(
        to_tensor(observations),
        actor(to_tensor(observations))
    )

    policy_loss = torch.mean(policy_loss * weights)
    policy_loss.backward()

    torch.nn.utils.clip_grad_norm(actor.parameters(), 10.)
    actor_optim.step()

    # Target update
    soft_update(target_actor, actor, tau)
    soft_update(target_critic, critic, tau)

    metrics = {
        "value_loss": value_loss,
        "policy_loss": policy_loss
    }

    with torch.no_grad():
        td_v_values = critic(
            to_tensor(observations, volatile=True, requires_grad=False),
            to_tensor(actions, volatile=True, requires_grad=False))
    td_error = td_target - td_v_values

    info = {
        "td_error": to_numpy(td_error)
    }

    return metrics, info


def train_single_thread(actor, critic, target_actor, target_critic, actor_optim, critic_optim, criterion,
                        args, global_episode, global_update_step, episodes_queue):
    workerseed = args.seed + 241 * args.thread
    set_global_seeds(workerseed)

    args.logdir = "{}/thread_{}".format(args.logdir, args.thread)
    create_if_need(args.logdir)

    logger = Logger(args.logdir)

    buffer = create_buffer(args.buffer_size)

    beta_deacy_fn = create_linear_decay_fn(
        initial_value=0.4,
        final_value=1.0,
        max_step=args.max_episodes)

    update_step = 0
    received_examples = 1
    while global_episode.value < args.max_episodes * (args.num_threads - 1):
        while True:
            try:
                replay = episodes_queue.get_nowait()
                for (observation, action, reward, next_observation, done) in replay:
                    buffer.add(observation, action, reward, next_observation, done)
                received_examples += len(replay)
            except py_queue.Empty:
                break

        if len(buffer) >= args.train_steps:
            beta = min(1.0, max(0.4, beta_deacy_fn(global_episode.value)))
            (tr_observations, tr_actions, tr_rewards, tr_next_observations, tr_dones, weights, batch_idxes) = \
                buffer.sample(batch_size=args.batch_size, beta=beta)

            step_metrics, step_info = update_fn(actor, critic, target_actor, target_critic, actor_optim, critic_optim,
                                                criterion, tr_observations, tr_actions, tr_rewards,
                                                tr_next_observations, tr_dones, weights, args.tau)

            update_step += 1
            global_update_step.value += 1

            new_priorities = np.abs(step_info["td_error"]) + 1e-6
            buffer.update_priorities(batch_idxes, new_priorities)

            for key, value in step_metrics.items():
                value = to_numpy(value)
                logger.scalar_summary(key, value, update_step)

        else:
            time.sleep(1)

        logger.scalar_summary("buffer size", len(buffer), global_episode.value)
        logger.scalar_summary(
            "updates per example",
            update_step * args.batch_size / received_examples,
            global_episode.value)

    save_fn(actor, critic, target_actor, target_critic, args.logdir, update_step)

    raise KeyboardInterrupt


def play_single_thread(actor, critic, target_actor, target_critic, args, global_episode,
                       episodes_queue, best_reward):
    workerseed = args.seed + 241 * args.thread
    set_global_seeds(workerseed)

    args.logdir = "{}/thread_{}".format(args.logdir, args.thread)
    create_if_need(args.logdir)

    logger = Logger(args.logdir)
    env = create_env(args.highknee, args.integrator_accuracy, args.skip_frames)
    random_process = create_ou_process(args.n_action)

    epsilon_cycle_len = random.randint(1e2, 4e2)

    epsilon_decay_fn = create_cycle_decay_fn(
        initial_value=1.0,
        final_value=0.01,
        cycle_len=epsilon_cycle_len,
        num_cycles=args.max_episodes // epsilon_cycle_len)

    episode = 1
    step = 0
    start_time = time.time()
    while global_episode.value < args.max_episodes * (args.num_threads - 1):
        if episode % 100 == 0:
            env = create_env(args.highknee, args.integrator_accuracy, args.skip_frames)
        seed = random.randrange(2 ** 32 - 2)

        epsilon = min(1.0, max(0.01, epsilon_decay_fn(episode)))

        episode_metrics = {
            "reward": 0.0,
            "step": 0,
            "epsilon": epsilon
        }

        observation = env.reset()
        random_process.reset_states()
        done = False

        replay = []
        while not done:
            action = act_fn(actor, observation, noise=epsilon * random_process.sample())
            next_observation, reward, done, _ = env.step(action)

            replay.append((observation, action, reward, next_observation, done))
            episode_metrics["reward"] += reward
            episode_metrics["step"] += 1

            observation = next_observation

        episodes_queue.put(replay)

        episode += 1
        global_episode.value += 1

        if episode_metrics["reward"] > best_reward.value:
            best_reward.value = episode_metrics["reward"]
            logger.scalar_summary("best reward", best_reward.value, episode)
            save_fn(actor, critic, target_actor, target_critic, args.logdir, episode)

        step += episode_metrics["step"]
        elapsed_time = time.time() - start_time

        for key, value in episode_metrics.items():
            logger.scalar_summary(key, value, episode)
        logger.scalar_summary(
            "episode per minute",
            episode / elapsed_time * 60,
            episode)
        logger.scalar_summary(
            "step per second",
            step / elapsed_time,
            episode)

    raise KeyboardInterrupt
