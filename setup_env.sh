#!/usr/bin/env bash

read -p "name of the environment : " env_name

conda create -n $env_name -c kidzik opensim git python=3.6 anaconda -y

conda activate $env_name

conda upgrade pip -y
conda install -c conda-forge lapack git -y
conda install ipython libgcc -y
conda install pytorch torchvision cpuonly -c pytorch -y

pip install tensorboardX tensorboard
pip install gym
pip install git+https://github.com/stanfordnmbl/osim-rl.git
